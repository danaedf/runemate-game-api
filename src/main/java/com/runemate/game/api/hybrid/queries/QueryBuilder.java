package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;
import lombok.*;

public abstract class QueryBuilder<T, QB extends QueryBuilder, QR extends QueryResults> implements Cloneable {
    final Object LOCK = new Object();
    private ConcurrentMap<String, Object> cache;
    private Predicate<T> filter;
    private Callable<List<? extends T>> provider;

    @SneakyThrows(CloneNotSupportedException.class)
    @Override
    public QB clone() {
        return (QB) super.clone();
    }

    public final QB filter(Predicate<T> filter) {
        if (this.filter == null) {
            this.filter = filter;
        } else {
            this.filter = this.filter.and(filter);
        }
        return get();
    }

    /**
     * Gets a reference to this with the correct type
     */
    public abstract QB get();

    public final QB provider(Callable<List<? extends T>> provider) {
        this.provider = provider;
        return get();
    }

    /**
     * Gets a QueryResults object from the available provider and lazily evaluates all prior chained statements.
     * The results need not be ordered.
     *
     * @return A non-null instance of {@link QR}
     */
    @SneakyThrows(Exception.class)
    public QR results() {
        return results(false);
    }

    /**
     * Gets a QueryResults object from the available provider and lazily evaluates all prior chained statements.
     * The results may or may not be ordered depending on
     *
     * @param ordered whether or not they must be in the original order
     * @return A non-null instance of {@link QR}
     */
    @SneakyThrows(Exception.class)
    protected final QR results(boolean ordered) {
        List<? extends T> provided = (provider != null ? provider : getDefaultProvider()).call();
        if (provided == null) {
            throw new IllegalArgumentException(
                "The provider, whether the default or a custom implementation, may not return a null List<? extends T>");
        }
        if (provided.isEmpty()) {
            getCache().clear();
            return results(provided);
        }


        Stream<? extends T> stream = provided.stream().filter(this::accepts);
        QR results = results(ordered ? stream.collect(Collectors.toList()) : stream.collect(Collectors.toSet()));
        getCache().clear();
        return results;
    }

    protected final QR results(Collection<? extends T> entries) {
        return results(entries, cache);
    }

    /**
     * Gets an instance of QueryResults containing the provided data entries.
     */
    protected abstract QR results(
        Collection<? extends T> entries,
        ConcurrentMap<String, Object> cache
    );

    /**
     * Gets the default provider of the unfiltered data entries. Must be thread-safe.
     *
     * @return A non-null {@link Callable} provider that is used by default.
     */
    public abstract Callable<List<? extends T>> getDefaultProvider();

    /**
     * Checks if the current builder accepts the argument
     *
     * @param argument The argument to be tested against the built query.
     * @return true if the provided argument is accepted by the built query.
     */
    public boolean accepts(final T argument) {
        return filter == null || filter.test(argument);
    }

    /**
     * Gets the cache as a ConcurrentMap that's used internally to speed up queries. Extreme caution should be taken when considering using it in any way. This should typically not be used by any bot. @return a ConcurrentMap
     */
    public final ConcurrentMap<String, Object> getCache() {
        return getCache(16);
    }

    /**
     * @see QueryBuilder#getCache
     */
    public final void setCache(ConcurrentMap<String, Object> cache) {
        this.cache = cache;
    }

    private synchronized ConcurrentMap<String, Object> getCache(int initialCapacity) {
        if (cache == null) {
            cache = new ConcurrentHashMap<>(initialCapacity);
        }
        return cache;
    }
}
package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import lombok.*;

@Getter
public class FrameMapDefinition implements DecodedItem {

    private final int id;
    private int[] types;
    private int[][] frameMaps;
    private int length;

    public FrameMapDefinition(int id) {
        this.id = id;
    }
    
    @Override
    public void decode(final Js5InputStream in) throws IOException {
        length = in.readUnsignedByte();
        types = new int[length];
        frameMaps = new int[length][];

        for (int i = 0; i < length; ++i)
        {
            types[i] = in.readUnsignedByte();
        }

        for (int i = 0; i < length; ++i)
        {
            frameMaps[i] = new int[in.readUnsignedByte()];
        }

        for (int i = 0; i < length; ++i)
        {
            for (int j = 0; j < frameMaps[i].length; ++j)
            {
                frameMaps[i][j] = in.readUnsignedByte();
            }
        }
    }
}

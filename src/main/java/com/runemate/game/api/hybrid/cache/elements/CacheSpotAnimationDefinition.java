package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.awt.*;
import java.io.*;
import java.util.*;

public class CacheSpotAnimationDefinition extends IncrementallyDecodedItem {
    private final boolean rs3;
    private final int id;
    public int modelId;
    private int animationId = -1;
    private int scaleXZ = 128;
    private int scaleY = 128;
    /**
     * Appears to actually be 0, 90, 180, 270, etc
     */
    private int angle = 0;//or rotation or orientation?
    private int dLightness = 0;
    private int dIntensity = 0;
    private short[] originalColors;
    private short[] replacementColors;
    private short[] originalTextureIds;
    private short[] replacementTextureIds;
    private byte aByte260;
    private int anInt265;
    private boolean aBoolean267;
    private byte[] aByteArray8534;
    private byte[] aByteArray8537;

    public CacheSpotAnimationDefinition(boolean rs3, int id) {
        this.rs3 = rs3;
        this.id = id;
    }

    @Override
    protected void decode(Js5InputStream buffer, int opcode) throws IOException {
        if (opcode == 1) {
            if (rs3) {
                this.modelId = buffer.readDefaultableUnsignedSmart();
            } else {
                this.modelId = buffer.readUnsignedShort();
            }
        } else if (opcode == 2) {
            if (rs3) {
                this.animationId = buffer.readDefaultableUnsignedSmart();
            } else {
                this.animationId = buffer.readUnsignedShort();
            }
        } else if (opcode == 4) {
            this.scaleXZ = buffer.readUnsignedShort();
        } else if (opcode == 5) {
            this.scaleY = buffer.readUnsignedShort();
        } else if (opcode == 6) {
            this.angle = buffer.readUnsignedShort();
        } else if (opcode == 7) {
            this.dLightness = buffer.readUnsignedByte();
        } else if (opcode == 8) {
            this.dIntensity = buffer.readUnsignedByte();
        } else if (opcode == 9) {
            aByte260 = 3;
            anInt265 = 8224;
        } else if (opcode == 10) {
            aBoolean267 = true;
        } else if (opcode == 11) {
            aByte260 = 1;
        } else if (opcode == 12) {
            aByte260 = 4;
        } else if (opcode == 13) {
            aByte260 = 5;
        } else if (opcode == 14) {
            aByte260 = 2;
            anInt265 = buffer.readUnsignedByte() << 8;
        } else if (opcode == 15) {
            aByte260 = 3;
            anInt265 = buffer.readUnsignedShort();
        } else if (opcode == 16) {
            aByte260 = 3;
            anInt265 = buffer.readInt();
        } else if (opcode == 40) {
            int var4 = buffer.readUnsignedByte();
            this.originalColors = new short[var4];
            this.replacementColors = new short[var4];

            for (int var5 = 0; var5 < var4; ++var5) {
                this.originalColors[var5] = (short) buffer.readUnsignedShort();
                this.replacementColors[var5] = (short) buffer.readUnsignedShort();
            }
        } else if (opcode == 41) {
            int var4 = buffer.readUnsignedByte();
            this.originalTextureIds = new short[var4];
            this.replacementTextureIds = new short[var4];

            for (int var5 = 0; var5 < var4; ++var5) {
                this.originalTextureIds[var5] = (short) buffer.readUnsignedShort();
                this.replacementTextureIds[var5] = (short) buffer.readUnsignedShort();
            }
        } else if (44 == opcode) {
            int i_5_ = buffer.readUnsignedShort();
            int i_6_ = 0;
            for (int i_7_ = i_5_; i_7_ > 0; i_7_ >>= 1) {
                i_6_++;
            }
            aByteArray8534 = new byte[i_6_];
            byte i_8_ = 0;
            for (int i_9_ = 0; i_9_ < i_6_; i_9_++) {
                if ((i_5_ & 1 << i_9_) > 0) {
                    aByteArray8534[i_9_] = i_8_;
                    i_8_++;
                } else {
                    aByteArray8534[i_9_] = -1;
                }
            }
        } else if (45 == opcode) {
            int i_10_ = buffer.readUnsignedShort();
            int i_11_ = 0;
            for (int i_12_ = i_10_; i_12_ > 0; i_12_ >>= 1) {
                i_11_++;
            }
            aByteArray8537 = new byte[i_11_];
            byte i_13_ = 0;
            for (int i_14_ = 0; i_14_ < i_11_; i_14_++) {
                if ((i_10_ & 1 << i_14_) > 0) {
                    aByteArray8537[i_14_] = i_13_;
                    i_13_++;
                } else {
                    aByteArray8537[i_14_] = -1;
                }
            }
        }
    }

    public SpotAnimationDefinition extended() {
        return new Extended();
    }

    public class Extended extends SpotAnimationDefinition {
        @Override
        public int getId() {
            return id;
        }

        @Override
        public int getModelId() {
            return modelId;
        }

        @Override
        public int getAnimationId() {
            return animationId;
        }

        @Override
        public int getAngle() {
            return angle;
        }

        @Override
        public Map<Color, Color> getColorSubstitutions() {
            if (originalColors == null) {
                return Collections.emptyMap();
            }
            Map<Color, Color> colorMapping = new HashMap<>(originalColors.length);
            for (int i = 0; i < originalColors.length; ++i) {
                colorMapping.put(
                    RSColors.fromHSV(originalColors[i]),
                    RSColors.fromHSV(replacementColors[i])
                );
            }
            return colorMapping;
        }

        @Override
        public Map<Material, Material> getMaterialSubstitutions() {
            if (originalTextureIds == null) {
                return Collections.emptyMap();
            }
            Map<Material, Material> materialMapping = new HashMap<>(originalTextureIds.length);
            for (int i = 0; i < originalTextureIds.length; ++i) {
                materialMapping.put(
                    Materials.load(originalTextureIds[i]),
                    Materials.load(replacementTextureIds[i])
                );
            }
            return materialMapping;
        }

        @Override
        public String toString() {
            return "SpotAnimationDefinition(id: " + id + ", modelId: " + modelId +
                ", animationId: " + animationId + ')';
        }
    }
}

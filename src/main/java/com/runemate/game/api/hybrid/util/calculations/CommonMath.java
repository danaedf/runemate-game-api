package com.runemate.game.api.hybrid.util.calculations;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.math.*;
import java.util.concurrent.*;

public final class CommonMath {
    private static final int[] BIT_MASKS = new int[32];

    static {
        int mask = 2;
        for (int bit = 0; bit < 32; ++bit) {
            BIT_MASKS[bit] = mask - 1;
            mask += mask;
        }
    }

    private CommonMath() {
    }

    /**
     * Converts a fraction into a percentage
     *
     * @param numerator   numerator
     * @param denominator denominator
     * @return the calculated percentage
     */
    public static int fractionToPercent(final long numerator, final long denominator) {
        return (int) Math.ceil((numerator / (double) denominator) * 100);
    }

    /**
     * Normalizes a given value to a value between 0.00 and 1.00
     *
     * @param current The current value
     * @param minimum The minimum value
     * @param maximum The maximum value
     * @return A normalized value
     */
    public static double normalize(
        final double current, final double minimum,
        final double maximum
    ) {
        return (current - minimum) / (maximum - minimum);
    }

    /**
     * Rounds a double to a specified amount of significant digits.
     * If the number is infinite or NaN, then it's returned unmodified.
     */
    public static double round(final double number, final int significantPlaces) {
        if (Double.isInfinite(number) || Double.isNaN(number)) {
            return number;
        }
        return new BigDecimal(number).setScale(significantPlaces, RoundingMode.HALF_UP)
            .doubleValue();
    }

    /**
     * @param unit    the time unit
     * @param runtime the runtime in milliseconds
     * @param value   the value to calculate the rate of
     * @return the amount of value collected during runtime at the rate of unit
     */
    public static double rate(TimeUnit unit, long runtime, double value) {
        if (unit == null || runtime <= 0 || value == 0) {
            return 0;
        }
        double nano = 0.0000001D;
        double micro = nano * 10000.00D;
        double millis = micro * 1000.00D;
        double second = millis * 1000.00D;
        double minute = second * 60.00D;
        double hour = minute * 60.00D;
        double day = hour * 24.00D;
        double[] measures = { nano, micro, millis, second, minute, hour, day };
        return (value * measures[unit.ordinal()]) / runtime;
    }

    /**
     * Gets the minimum distance between the two angles.
     * For example, if the angles are 25 and 290, the result would be 95.
     *
     * @param angleA
     * @param angleB
     * @return
     */
    public static int getDistanceBetweenAngles(int angleA, int angleB) {
        int angle = Math.abs(angleA - angleB);
        if (angle > 180) {
            angle = 360 - angle;
        }
        return angle;
    }

    /**
     * Gets the angle of the target entity using the local player as the origin
     *
     * @param target
     * @return
     */
    public static int getAngleOf(Locatable target) {
        return getAngleOf(Players.getLocal(), target);
    }

    /**
     * Gets the angle of the target entity from the provided origin.
     *
     * @param origin
     * @param target
     * @return
     */
    public static int getAngleOf(Locatable origin, Locatable target) {
        if (origin == null) {
            return -1;
        }
        Coordinate oc = origin.getPosition();
        if (oc == null) {
            return -1;
        }
        if (target == null) {
            return -1;
        }
        Coordinate tc = target.getPosition();
        if (tc == null) {
            return -1;
        }
        int angle =
            ((int) Math.toDegrees(StrictMath.atan2(tc.getY() - oc.getY(), tc.getX() - oc.getX()))) -
                90;
        if (angle < 0) {
            angle += 360;
        }
        return angle % 360;
    }

    public static int[] getBitMasks() {
        return BIT_MASKS.clone();
    }

    public static int getBitMask(int length) {
        return BIT_MASKS[length - 1];
    }

    public static int getBitRange(int value, int leastSignificantBit, int mostSignificantBit) {
        return value == -1 ? -1 :
            (value >> leastSignificantBit) & BIT_MASKS[mostSignificantBit - leastSignificantBit];
    }
}

package com.runemate.game.api.hybrid.entities.status;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;
import java.util.*;
import lombok.*;

@Value
public class Hitsplat implements Validatable {

    int id;
    int damage;
    int endCycle;
    int secondaryId;
    int secondaryDamage;

    @Deprecated
    public int geId() {
        return id;
    }

    @Deprecated
    public int getStartCycle() {
        return -1;
    }

    @NonNull
    public Classification getClassification() {
        return Arrays.stream(Classification.values())
            .filter(classification -> classification.id == id)
            .findFirst()
            .orElse(Classification.UNCLASSIFIED);
    }

    @Override
    public boolean isValid() {
        return endCycle > RuneScape.getCurrentCycle();
    }

    @Getter
    public enum Classification {

        //Events triggered by (or against) the local player
        BLOCK_ME(12, true),
        DAMAGE_ME(16, true),
        DAMAGE_ME_CYAN(18, true),
        DAMAGE_ME_ORANGE(20, true),
        DAMAGE_ME_YELLOW(22, true),
        DAMAGE_ME_WHITE(24, true),
        DAMAGE_MAX_ME(43, true),
        DAMAGE_MAX_ME_CYAN(44, true),
        DAMAGE_MAX_ME_ORANGE(45, true),
        DAMAGE_MAX_ME_YELLOW(46, true),
        DAMAGE_MAX_ME_WHITE(47, true),
        DAMAGE_ME_POISE(53, true),
        DAMAGE_MAX_ME_POISE(55, true),

        //Events triggered by other entities
        BLOCK_OTHER(13, false),
        DAMAGE_OTHER(17, false),
        DAMAGE_OTHER_CYAN(19, false),
        DAMAGE_OTHER_ORANGE(21, false),
        DAMAGE_OTHER_YELLOW(23, false),
        DAMAGE_OTHER_WHITE(25, false),
        DAMAGE_OTHER_POISE(54, false),

        //Remaining
        POISON(65),
        DISEASE(4),
        VENOM(5),
        HEALING(6),
        CYAN_UP(11),
        CYAN_DOWN(15),
        PRAYER_DRAIN(60),
        BLEED(67),
        SANITY_DRAIN(71),
        SANITY_RESTORE(72),
        DOOM(73),
        BURN(74),
        CORRUPTION(0),
        @Deprecated MISS(0),
        @Deprecated COMBAT(1),

        UNCLASSIFIED(),

        //RS3
        @Deprecated MELEE_AUTO_ATTACK(132),
        @Deprecated MELEE_ABILITY(133),
        @Deprecated MELEE_CRITICAL_HIT(134),
        @Deprecated RANGED_AUTO_ATTACK(135),
        @Deprecated RANGED_ABILITY(136),
        @Deprecated RANGED_CRITICAL_HIT(137),
        @Deprecated MAGIC_AUTO_ATTACK(138),
        @Deprecated MAGIC_ABILITY(139),
        @Deprecated MAGIC_CRITICAL_HIT(140),
        @Deprecated TYPELESS(144),
        @Deprecated BIG_GAME_HUNTER_BLUE(233),
        @Deprecated BIG_GAME_HUNTER_YELLOW(235),
        @Deprecated BIG_GAME_HUNTER_RED(250);

        private final int id;
        private final boolean players;
        @Deprecated
        private final int[] classifiers;

        Classification(int id, boolean players) {
            this.id = id;
            this.players = players;
            this.classifiers = new int[] { id };
        }

        Classification(int id) {
            this(id, false);
        }

        Classification() {
            this(-1);
        }

    }

}

package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;

public final class WorldSelect {
    private WorldSelect() {
    }

    public static boolean isOpen() {
        return OSRSWorldSelect.isOpen();
    }

    public static boolean open() {
        return OSRSWorldSelect.open();
    }

    public static boolean isSelected(int world) {
        return OSRSWorldSelect.isSelected(world);
    }

    public static boolean select(final int world) {
        return OSRSWorldSelect.select(world);
    }

    public static boolean isSelectable(int world) {
        WorldOverview overview = Worlds.getOverview(world);
        return overview != null;
    }
}

package com.runemate.game.api.hybrid.cache.sprites;

import java.awt.image.*;
import java.util.*;

public class Sprite {
    private final int width;
    private final int height;
    private final List<BufferedImage> sequencedFrames;

    public Sprite(int width, int height) {
        this(width, height, 1);
    }

    public Sprite(int width, int height, int size) {
        if (size < 1) {
            throw new IllegalArgumentException();
        }
        this.width = width;
        this.height = height;
        this.sequencedFrames = new ArrayList<>(size);
    }
}

package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.materials.*;

public interface UnderlayDefinition {
    int getId();

    Material getMaterial();
}

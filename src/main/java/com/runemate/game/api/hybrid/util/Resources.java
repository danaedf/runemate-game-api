package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.exception.*;
import com.runemate.io.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.util.*;
import javax.imageio.*;
import lombok.extern.log4j.*;

@Log4j2
public final class Resources {

    public static URL getAsURL(String resource, AbstractBot bot) {
        if (bot == null || resource == null) {
            return null;
        }

        if (isNotDeclared(resource, bot)) {
            throw new ResourceNotDeclaredException(resource);
        }

        ClassLoader loader = bot.getClass().getClassLoader();
        return loader.getResource(resource);
    }

    public static URL getAsURL(String resource) {
        return getAsURL(resource, Environment.getBot());
    }

    public static InputStream getAsStream(AbstractBot bot, String resource) {
        if (bot == null || resource == null) {
            return null;
        }

        if (isNotDeclared(resource, bot)) {
            throw new ResourceNotDeclaredException(resource);
        }

        ClassLoader loader = bot.getClass().getClassLoader();
        return loader.getResourceAsStream(resource);
    }

    public static InputStream getAsStream(String name) {
        return getAsStream(Environment.getBot(), name);
    }

    public static byte[] getAsByteArray(AbstractBot bot, String name) throws IOException {
        InputStream stream = getAsStream(bot, name);
        if (stream == null) {
            return null;
        }

        try (IOTunnel tunnel = new IOTunnel(stream)) {
            return tunnel.readAsArray();
        }
    }

    public static byte[] getAsByteArray(String name) throws IOException {
        return getAsByteArray(Environment.getBot(), name);
    }

    public static String getAsString(AbstractBot bot, String name, Charset encoding) throws IOException {
        byte[] result = getAsByteArray(bot, name);
        if (result == null) {
            return null;
        }
        return new String(result, encoding);
    }

    public static String getAsString(String name, Charset encoding) throws IOException {
        return getAsString(Environment.getBot(), name, encoding);
    }

    public static BufferedImage getAsBufferedImage(AbstractBot bot, String name) throws IOException {
        InputStream stream = getAsStream(bot, name);
        if (stream == null) {
            return null;
        }
        return ImageIO.read(stream);
    }

    public static BufferedImage getAsBufferedImage(String name) throws IOException {
        return getAsBufferedImage(Environment.getBot(), name);
    }

    public static SerializableWeb getAsSerializedWeb(AbstractBot bot, String name)
        throws IOException, ClassNotFoundException {
        byte[] result = getAsByteArray(bot, name);
        if (result == null) {
            return null;
        }
        return SerializableWeb.deserialize(result);
    }

    public static SerializableWeb getAsSerializedWeb(String name)
        throws IOException, ClassNotFoundException {
        return getAsSerializedWeb(Environment.getBot(), name);
    }

    private static boolean isNotDeclared(String name, AbstractBot bot) {
        //Assume it is declared if we're running from the store
        //The support URL check is required since the dev toolkit can be run in dev mode
        if (!Environment.isDevMode() || bot.getMetadata().getSupportUrl() != null) {
            return false;
        }

        for (String declared : bot.getMetadata().getDeclaredResources()) {
            if (declared.equals(name)) {
                return false;
            }

            String file = Arrays.stream(name.split("/")).reduce((f, s) -> s).orElse(name);
            if (declared.endsWith("/")) { //declaration is a folder or declaration is explicit
                return false;
            }
        }
        return true;
    }
}

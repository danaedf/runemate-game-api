package com.runemate.game.api.hybrid.web.vertex;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.web.*;
import java.util.*;
import org.jetbrains.annotations.*;

public interface Vertex extends Locatable {

    boolean step(Map<String, Object> cache);

    ScanResult scan(Map<String, Object> cache);

    @Nullable
    @Override
    default Coordinate.HighPrecision getHighPrecisionPosition() {
        final var position = getPosition();
        return position != null ? position.getHighPrecisionPosition() : null;
    }

    @Nullable
    @Override
    default Area.Rectangular getArea() {
        final var position = getPosition();
        return position != null ? position.getArea() : null;
    }
}

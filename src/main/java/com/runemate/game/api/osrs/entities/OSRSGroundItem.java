package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.rmi.*;
import java.awt.*;
import java.util.*;
import java.util.regex.*;
import org.jetbrains.annotations.*;

public final class OSRSGroundItem extends OSRSCacheModelEntity implements GroundItem {

    private final Coordinate position;
    private boolean modelLoaded;
    private final OpenItemNode node;
    private GroundItem.Ownership ownership;

    public OSRSGroundItem(OpenItemNode node, Coordinate position) {
        super(node.getUid());
        this.node = node;
        this.ownership = Ownership.of(node.getOwnership());
        this.position = position;
    }

    @Override
    public int getId() {
        return node.getId();
    }

    @Override
    public int getQuantity() {
        return node.getStackSize();
    }

    @Override
    public ItemDefinition getDefinition() {
        return ItemDefinition.get(getId());
    }

    @Override
    public boolean take() {
        final var definition = getDefinition();
        return definition != null && interact("Take", definition.getName());
    }

    @Override
    public boolean interact(@Nullable final Pattern action) {
        if (Magic.getSelected() == null && Inventory.getSelectedItem() == null) {
            final var definition = getDefinition();
            if (definition != null) {
                //On RuneLite the GroundItem plugin will append then quantity, so we should optionally match against "$name ($quantity)"
                //e.g. "Iron arrows" vs "Iron Arrows (14)"
                return interact(action, Regex.getPatternForExactString(definition.getName(), true));
            }
        }
        return super.interact(action);
    }

    @Override
    public Ownership getOwnership() {
        if (ownership == Ownership.GROUP) {
            AccountInfo.AccountType type = AccountInfo.getType();
            if (type != null && !type.isGroupIronman()) { //Non-GIMs see loot from other people as 'GROUP'
                ownership = Ownership.OTHER;
            }
        }
        return ownership;
    }

    @Nullable
    private Model getRemoteModel() {
        final var shape = getShape();
        return shape != null ? new RemoteModel(this, shape) : null;
    }

    private Shape getShape() {
        return OpenHull.lookup(uid, node.getX(), node.getY());
    }

    @Override
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        if (cacheModel != null) {
            return cacheModel;
        }
//        Model remote = getRemoteModel();
//        if (remote != null) {
//            return remote;
//        }
        if (!modelLoaded) {
            modelLoaded = true;
            CacheItemDefinition.Extended def = (CacheItemDefinition.Extended) getDefinition();
            if (def != null) {
                int heightOffset = -node.getItemPileElevation();
                int modelId = def.getGroundModelId();
                int quantity = getQuantity();
                if (quantity > 1) {
                    ItemDefinition substitution = def.getSubstitution(quantity);
                    if (substitution != null) {
                        modelId = substitution.getGroundModelId();
                    }
                }
                if (modelId >= 0) {
                    CacheModel component = CacheModel.load(def.getGroundModelId());
                    if (component != null) {
                        CompositeCacheModel model = new CompositeCacheModel(this, heightOffset, Collections.singletonList(component));
                        model.setScale(def.getModelXScale(), def.getModelYScale(), def.getModelZScale());
                        return cacheModel = model;
                    }
                }
            }
        }
        return backupModel;
    }

    @Override
    public Coordinate getPosition(Coordinate regionBase) {
        return isValid() ? position : null;
    }

    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        final Coordinate base = getPosition(regionBase);
        if (base != null) {
            return new Coordinate.HighPrecision((base.getX() << 7) + 64, (base.getY() << 7) + 64, base.getPlane());
        }
        return null;
    }

    @Override
    public boolean interact(final String action) {
        return interact(action, (Pattern) null);
    }


    @Override
    public boolean isValid() {
        return node.getNext() != 0;
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof OSRSGroundItem) {
            final OSRSGroundItem casted = (OSRSGroundItem) object;
            if (BridgeUtil.getFieldInstance(uid) == BridgeUtil.getFieldInstance(casted.uid)) {
                return true;
            }
            return getId() == casted.getId() && getQuantity() == casted.getQuantity() && Objects.equals(getArea(), casted.getArea());
        }
        return false;
    }

    @Override
    public String toString() {
        String wip = null;
        if (BotPlatform.isBotThread()) {
            ItemDefinition def = getDefinition();
            if (def != null) {
                wip = def.getName();
            }
        }
        if (wip == null) {
            wip = Integer.toString(getId());
        }
        int quantity = getQuantity();
        if (quantity != 1) {
            wip += " x " + quantity;
        }
        return wip + " [" + position.getX() + ", " + position.getY() + ", " + position.getPlane() + ']';
    }
}

package com.runemate.game.api.osrs.local;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import lombok.extern.log4j.*;

@Log4j2
public final class SpecialAttack {

    public static int getEnergy() {
        return Varps.getAt(300).getValue() / 10;
    }

    public static boolean isActivated() {
        return Varps.getAt(301).getValue() == 1;
    }

    public static boolean activate(boolean useCombatOptions) {
        log.info("Activating special attack using {}", useCombatOptions ? "bar" : "orb");
        return isActivated() || useCombatOptions ? toggleWithBar() : toggleWithOrb();
    }

    public static boolean activate() {
        return activate(!PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
    }

    public static boolean deactivate(boolean useCombatOptions) {
        return !isActivated() || useCombatOptions ? toggleWithBar() : toggleWithOrb();
    }

    public static boolean deactivate() {
        return deactivate(!PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
    }

    private static boolean toggleWithOrb() {
        final InterfaceComponent component = Interfaces.newQuery()
            .containers(160)
            .types(InterfaceComponent.Type.CONTAINER)
            .actions("Use")
            .names("Special Attack")
            .grandchildren(false)
            .results()
            .first();
        return component != null && component.interact("Use");
    }

    private static boolean toggleWithBar() {
        if (!ControlPanelTab.COMBAT_OPTIONS.open()) {
            return false;
        }
        final InterfaceComponent component = Interfaces.newQuery()
            .containers(593)
            .types(InterfaceComponent.Type.CONTAINER)
            .actions("Use Special Attack")
            .grandchildren(false)
            .results()
            .first();
        return component != null && component.interact("Use Special Attack");
    }

}

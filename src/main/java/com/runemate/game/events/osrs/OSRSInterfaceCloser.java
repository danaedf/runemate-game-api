package com.runemate.game.events.osrs;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.events.common.*;

public class OSRSInterfaceCloser extends InterfaceCloser {
    private final UnwantedInterface[] UNWANTED_INTERFACES = {
        new UnwantedInterface(
            "Collect From Grand Exchange",
            Interfaces.newQuery().containers(402).types(InterfaceComponent.Type.SPRITE)
                .actions("Close"),
            OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()
        ),
        new UnwantedInterface(
            "Report Abuse Interface",
            Interfaces.newQuery().containers(553).types(InterfaceComponent.Type.SPRITE)
                .actions("Close"),
            OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()
        ),
        new UnwantedInterface(
            "Poll Booth",
            Interfaces.newQuery().containers(345, 310).types(InterfaceComponent.Type.SPRITE)
                .actions("Close"),
            OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()
        ),
        new UnwantedInterface(
            "Skill Guides",
            Interfaces.newQuery().containers(214).types(InterfaceComponent.Type.SPRITE)
                .grandchildren(false).actions("Close"),
            OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()
        ),
        new UnwantedInterface(
            "Play Button",
            Interfaces.newQuery().containers(378).types(InterfaceComponent.Type.SPRITE).sprites(429)
                .grandchildren(false),
            false
        ),
        new UnwantedInterface(
            "Bank PIN Confirmation",
            Interfaces.newQuery().containers(14).types(InterfaceComponent.Type.LABEL)
                .texts("Yes, I asked for this. I want this PIN.").grandchildren(false),
            false
        ),
        new UnwantedInterface(
            "Dangerous Area Warning",
            Interfaces.newQuery().containers(565, 580).grandchildren(false)
                .types(InterfaceComponent.Type.LABEL)
                .texts("Proceed regardless", "Enter the swamp."),
            false
        ),
        //There is no reason we should close this by default. If the author wants to enter the wilderness they
        //can handle his interface themselves.
//        new UnwantedInterface(
//            "Entering Wilderness Warning",
//            Interfaces.newQuery().containers(475).grandchildren(2).grandchildren(false)
//                .types(InterfaceComponent.Type.CONTAINER).actions("Enter Wilderness"),
//            false
//        ),
        new UnwantedInterface(
            "Sophanem Dungeon Entrance Warning",
            Interfaces.newQuery().containers(562).grandchildren(false)
                .types(InterfaceComponent.Type.LABEL).actions("Yes"),
            false
        ),
        new UnwantedInterface(
            "Quest Complete Dialogue",
            Interfaces.newQuery().containers(277).grandchildren(false)
                .types(InterfaceComponent.Type.SPRITE).widths(26).heights(23),
            false
        )
    };

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.INTERFACE_CLOSER;
    }

    @Override
    public UnwantedInterface[] getUnwantedInterfaces() {
        return UNWANTED_INTERFACES;
    }
}

package com.runemate.bot.test;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.*;
import com.runemate.ui.setting.annotation.open.*;
import java.util.*;
import javafx.scene.control.*;
import lombok.extern.log4j.*;
import org.apache.commons.lang3.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
@Log4j2
public class FeatureTestBot extends LoopingBot implements EngineListener {

    @SettingsProvider(updatable = true)
    ExampleSettings settings;

    @Override
    public void onStart(final String... arguments) {
        getEventDispatcher().addListener(this);
        log.info("Registered {} with dispatcher", getClass().getName());
    }

    @Override
    public void onLoop() {
        log.info("Looping!");
    }

    @Override
    public void onEngineEvent(final EngineEvent event) {
        log.info("{}", event);
    }
}
